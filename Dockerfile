FROM ubuntu:20.04
WORKDIR /WebServer_1
COPY . /WebServer_1
RUN apt-get update -y
RUN apt-get install python3-dev -y
RUN apt install python3-pip -y
RUN pip3 install Flask
ENTRYPOINT ["python3"]
CMD ["prueba.py"]
# para ejecutar el flask necesitamos hacer en la carpeta del archivo .py
# export FLASK_APP=nombre.py
# flask run
